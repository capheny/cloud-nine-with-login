package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular_SpecialCharaters( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "y1yia0z" ) );
	}

	@Test
	public void testIsValidLoginBoundaryIn_SpecialCharaters( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r123456" ) );
	}

	@Test
	public void testIsValidLoginBoundaryOut_SpecialCharaters( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1yiyaoz" ) );
	}

	@Test
	public void testIsValidLoginException_SplCharacters( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Yiyao.$%" ) );
	}

	@Test
	public void testIsValidLoginRegular_Length( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "yiyaozhang" ) );
	}

	@Test
	public void testIsValidLoginBoundaryIn_Length( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "yiyaoz" ) );
	}

	@Test
	public void testIsValidLoginBoundaryOut_Length( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "yiyao" ) );
	}

	@Test
	public void testIsValidLoginException_Length( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "yz" ) );
	}

}
