package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if (loginName.length() < 6) {
			return false;
		}
		String regex = "^[a-zA-Z][a-zA-Z0-9]{5,}$";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(loginName);

		return matcher.matches();
	}
}
